# CarCar

Team:

* Person 1 - Linh Ngo (Sales microservice), (Inventory react files: ModelsForm, ModelsList, AutomobileForm)
* Person 2 - Mark Pastoral (Service microservice), (Inventory react files: ManufacturerForm, ManufacturerList, AutomobileList)
* Both: React: App.js, Nav.js
## Design

## Service microservice - Mark Pastoral
Models/Views
- AutomobileVO model: has vin, sold, import_href properties. Takes information from Automobile model in the inventory microservice using the poller file
    - User can get list of automobiles in the inventory
- Technician model: has first, last names, employee id
    - User can find a list of technicians in the Technicians page, or add a technician using the form page
    - Delete technician only for backend
- Appointment: has datetime, reason, status, vin, customer, technician(as foreignkey)
    - User can get list of appointments that excludes appointments with the status "cancelled" or "finished" in the Service appointments page
    - User can get list of all appointments found in the Service Appointment history page
    - User can schedule an appointment
        - If the vin the user enters has a status of "sold" in an automobile in inventory, React will change the user's VIP status in the appointments list and appointments history pages
    - User can cancel or finish an appointment on the Service Appointments page
        - each button changes the status of the appointment (default is "in process" in the backend, defined by the Appointments model)
React:
- Manufacturer form and list pages will use data from localhost:8100. The list displays all manufacturers' names and the form will let the user enter a new manufacturer
- AutomobileList will list all fetch data from localhost:8100. The list will display VIN/Color/Year/Model/Manufacturer/Sold status. The sold will display a certain string depending on if the automobile in the inventory has a sold status of 'true' or 'false'.




## Sales microservice - Linh Ngo

The sales microservice handles sales operations through the management of salespeople, customers, and sales records. We used React to build a user-friendly single page application to provide clean interfaces for viewing, creating, and deleting data within the sales microservices system.

Salesperson: includes fields for employee’s first name, last name, and employee ID. Each salesperson is uniquely identified by their employee ID.

Customer: includes information about customers and includes fields for their first name, last name, address, and phone number. It allows for the identification and contact details of customers.

Sale:  utilizes foreign keys and manages sales records and includes fields for the automobile, salesperson, customer, and price.

AutomobileVO: stores information about automobiles from the inventory service, includes fields such as the VIN, href, and sold status. The sales microservice polls data every 10 seconds from the automobiles inventory to the AutomobileVO. Since the AutomobileVO model has an initial value set to False (unsold), when it is updated to sold in the SalesForm the sold value will be updated to True.
