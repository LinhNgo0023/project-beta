import { useEffect, useState } from "react";

export default function AppointmentForm() {
    const [vin, setVin] = useState('')
    const [customer, setCustomer] = useState('')
    const [dateTime, setDateTime] = useState('')
    const [reason, setReason] = useState('')
    const [technician, setTechnician] = useState('')
    const [technicians, setTechnicians] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setTechnicians(data.technicians)
        }
    }

    const handleVinChange = async (event) => {
        const value = event.target.value
        setVin(value)
    }

    const handleCustomerChange = async (event) => {
        const value = event.target.value
        setCustomer(value)
    }

    const handleDateTimeChange = async (event) => {
        const value = event.target.value
        setDateTime(value)
    }

    const handleTechnicianChange = async (event) => {
        const value = event.target.value
        setTechnician(value)
    }

    const handleReasonChange = async (event) => {
        const value = event.target.value
        setReason(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.vin = vin
        data.customer = customer
        data.date_time = dateTime
        data.reason = reason
        data.technician = technician

        const appointmentUrl = 'http://localhost:8080/api/appointments/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(appointmentUrl, fetchConfig)
        if (response.ok) {
            setVin('')
            setCustomer('')
            setDateTime('')
            setReason('')
            setTechnician('')

        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className="container">
            <div className='row'>
                <div className="offset-3 col-6">
                    <div className="shadow p4 mt-4">
                        <h1>Schedule a new appointment</h1>
                        <form onSubmit={handleSubmit} id="create-appointment-form">
                            <div className="form-floating mb-3">
                                <input value={vin} onChange={handleVinChange} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                                <label htmlFor="vin">VIN</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={customer} onChange={handleCustomerChange} placeholder="customer name" required type="text" name="customer" id="customer" className="form-control" />
                                <label htmlFor="customer">Customer</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={dateTime} onChange={handleDateTimeChange} placeholder="date" required type="datetime-local" name="date" id="date" className="form-control" />
                                <label htmlFor="date">Date</label>
                            </div>
                            <div className="mb-3">
                                <select value={technician} onChange={handleTechnicianChange} placeholder="technician" required name="technician" id="technician" className="form-control">
                                    <option value="">Choose a technician owo!</option>
                                    {technicians.map(technician => {
                                        return (
                                            <option key={technician.id} value={technician.id}>
                                                {technician.first_name} {technician.last_name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={reason} onChange={handleReasonChange} placeholder="reason" required type="text" name="reason" id="reason" className="form-control" />
                                <label htmlFor="reason">Reason</label>
                            </div>
                            <button className="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )



}