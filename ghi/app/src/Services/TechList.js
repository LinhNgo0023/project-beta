import { React, useState, useEffect } from "react"

export default function TechList() {
    const [techList, setTechList] = useState([]);
    const fetchData = async () => {
        const url = "http://localhost:8080/api/technicians/"
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setTechList(data)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    const handleDelete = async (id) => {
        const response = await fetch(`http://localhost:8080/api/technicians/${id}/`, {
            method: 'delete',
        })
        if (response.ok) {
            fetchData()
        }
    }


    return (
        <div>
            <div>
                <h1>Technicians</h1>
            </div>
        <table className="table table-hover table-striped border border-5">
            <thead>
                <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
            </thead>
            <tbody>
                {techList.technicians?.map(technician => {
                    return (
                        <tr key={technician.id}>
                            <td className='text-center'>{technician.employee_id}</td>
                            <td className='text-center'>{technician.first_name}</td>
                            <td className='text-center'>{technician.last_name}</td>
                            <td>
                                <button type="button" className="btn btn-light" onClick={() => handleDelete(technician.id)}>Delete</button>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        </div>
    )


}