import { React, useState, useEffect } from "react"

export default function AppointmentHistoryList() {
    const [appointmentList, setAppointmentList] = useState([]);
    const [vin, setVin] = useState([])
    const [query, setQuery] = useState('')
    const [state, setState] = useState({
        query: "",
        list: appointmentList,
    })

    const fetchData = async () => {
        const url = "http://localhost:8080/api/appointments/history/"
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setAppointmentList(data.appointments)
            setState({
                query: '',
                list: data.appointments,
            })
        }
    }

    const fetchAutomobileData = async () => {
        const url = "http://localhost:8080/api/automobiles/"
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            let sold = []
            for (const auto of data.autos) {
                if (auto.sold === true) {
                    sold.push(auto.vin)
                }
            }
            setVin(sold)
        }
    }

    const vip = (vinNumber) => (vin.includes(vinNumber) ? 'yes :)' : 'no :(')

    const handleSearch = (event) => {
        event.preventDefault()
        const appointmentListFiltered = appointmentList.filter((appointment) => {
            return appointment.vin.toLowerCase().includes(query.toLowerCase())
        })
        setState({
            query: query,
            list: appointmentListFiltered,
        })
        setQuery('')

    }

    const handleSearchBar = (event) => {
        setQuery(event.target.value)
    }

    useEffect(() => {
        fetchData()
        fetchAutomobileData()
    }, [])

    return (
        <div>
            <div>
                <h1>Service Appointments History</h1>
            </div>
            <div>
                <form className="form-inline input-group" onSubmit={handleSearch}>
                <input className="form-control" type="search" name="search" id="search" placeholder="Search owo!" onChange={handleSearchBar} value={query} />
                <button className="btn btn-success">uwu</button>
                </form>
            </div>
            <table className="table table-hover table-striped border border-5">
                <thead>
                    <tr>
                        <th className='text-center'>Date Time</th>
                        <th className='text-center'>Customer</th>
                        <th className='text-center'>VIN</th>
                        <th className='text-center'>Reason</th>
                        <th className='text-center'>Status</th>
                        <th className='text-center'>Technician</th>
                        <th className="text-center">VIP</th>
                    </tr>
                </thead>
                <tbody>
                    {state.list?.map(appointment => {
                        return (
                            <tr key={appointment.id}>
                                <td className='text-center'>{appointment.date_time}</td>
                                <td className='text-center'>{appointment.customer}</td>
                                <td className='text-center'>{appointment.vin}</td>
                                <td className='text-center'>{appointment.reason}</td>
                                <td className='text-center'>{appointment.status}</td>
                                <td className='text-center'>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                <td className="text-center">{vip(appointment.vin)}</td>
                            </tr>
                        )
                    })}
                    
                </tbody>
            </table>
        </div>
    )

}