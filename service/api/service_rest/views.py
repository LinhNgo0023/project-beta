import json
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from pkg_resources import require
from service_rest.encoders import (TechnicianEncoder, AutomobileVOEncoder,
                                   AppointmentEncoder, Technician, 
                                   Appointment, AutomobileVO,
                                   )

# Create your views here.

@require_http_methods(["GET", "POST"])
def list_technician(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {'technicians': technicians },
            encoder=TechnicianEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician id does not exist owo;;;"},
                status=400
            )


@require_http_methods(["DELETE"])
def delete_technician(request, pk):
    if request.method == "DELETE":
        if pk == None:
            return JsonResponse
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({"deleted uwu!": count>0})
    

@require_http_methods(["GET"])
def list_automobiles(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"autos": automobiles},
            encoder=AutomobileVOEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def list_current_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.exclude(status="cancelled").exclude(status="finished")
        return JsonResponse(
            {'appointments': appointments},
            encoder=AppointmentEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician id does not exist uwu"},
                status=400,
            )
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def delete_appointment(request, pk):
    if request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted appointment": count>0})


@require_http_methods(["PUT"])
def cancel_finish_appointment(request, pk):
    try:
        content = json.loads(request.body)
        Appointment.objects.filter(id=pk).update(**content)
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    
    except Appointment.DoesNotExist:
        return JsonResponse(
            {"message": "Appointment does not exist ;-;"},
            status=400,
            )


def appointment_history(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
            safe=False,
        )
