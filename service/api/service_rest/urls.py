from .views import (list_technician, delete_technician, list_automobiles,
                    list_current_appointments, delete_appointment,
                    cancel_finish_appointment, appointment_history,
                    )
from django.urls import path

urlpatterns = [
    path("technicians/", list_technician, name="list_technician"),
    path("technicians/<int:pk>/", delete_technician, name="delete_technician"),
    path("automobiles/", list_automobiles, name="list_automobiles"),
    path("appointments/", list_current_appointments, name="list_all_appointments"),
    path("appointments/<int:pk>/", delete_appointment, name="delete_appointment"),
    path("appointments/history/", appointment_history, name="appointment_history"),
    path("appointments/<int:pk>/cancel/", cancel_finish_appointment, name="cancel_finish_appointment"),
    path("appointments/<int:pk>/finish/", cancel_finish_appointment, name="cancel_finish_appointment"),
]
